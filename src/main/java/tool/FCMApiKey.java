package tool;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by oleg on 19.06.17.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface FCMApiKey {
}
