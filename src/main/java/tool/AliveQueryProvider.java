package tool;

/**
 * Created by oleg on 08.07.17.
 */
public interface AliveQueryProvider {
    void keepAlive();
}
