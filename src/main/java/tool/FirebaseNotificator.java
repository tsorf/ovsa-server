package tool;

import com.google.gson.Gson;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by oleg on 19.06.17.
 */
public class FirebaseNotificator {

    public static final String FB_URL = "https://fcm.googleapis.com/fcm/send";

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private String apiKey;
    private Gson gson = new Gson();
    private OkHttpClient okHttpClient;

    @Inject
    public FirebaseNotificator(@FCMApiKey String apiKey) {
        this.apiKey = apiKey;
        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request.Builder builder = chain.request().newBuilder();
                    builder.addHeader("Authorization", "key=" + apiKey);
                    builder.addHeader("Content-Type", "application/json");
                    return chain.proceed(builder.build());
                })
                .build();
    }

    public void notify(String uid, Object data) {
        Message message = new Message(uid, data);

        RequestBody body = RequestBody.create(JSON, gson.toJson(message));
        Request request = new Request.Builder()
                .url(FB_URL)
                .post(body)
                .build();
        try {
            Response response = okHttpClient.newCall(request).execute();
            response.body().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Message {
        private String to;
        private Object data;

        public Message(String to, Object data) {
            this.to = to;
            this.data = data;
        }
    }
}
