package rest.tool;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by oleg on 01.07.16.
 */
public class DateTools {

    private static SimpleDateFormat dottedDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private static SimpleDateFormat dashedDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat dottedDateFormatWithTime = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private static SimpleDateFormat dayOfMonthFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());
    private static SimpleDateFormat prettyDateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());

    public static String formatDottedDateWithTime(Date date) {
        if(date == null) return null;
        return dottedDateFormatWithTime.format(date);
    }

    public static String formatTime(Date date) {
        if(date == null) return null;
        return timeFormat.format(date);
    }

    public static String formatDashedDate(Date date) {
        if(date == null) return null;
        return dashedDateFormat.format(date);
    }

    public static String formatPrettyDate(Date date) {
        if(date == null) return null;
        return prettyDateFormat.format(date);
    }

    public static String formatDottedDate(Date date) {
        if(date == null) return null;
        return dottedDateFormat.format(date);
    }

    public static Date parseDottedDate(String date) {
        if(date == null) return null;
        try {
            return dottedDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatDayOfMonth(Date date) {
        if(date == null) return null;
        return dayOfMonthFormat.format(date);
    }

    public static String formatTime(int hour, int minute) {
        String h = hour < 10 ? "0" + hour : String.valueOf(hour);
        String m = minute < 10 ? "0" + minute : String.valueOf(minute);
        return String.format("%s:%s", h, m);
    }


    public static String formatMillisecondsToTime(int time) {
        int minutes = time / (60 * 1000);
        int seconds = (time / 1000) % 60;
        return String.format("%d:%02d", minutes, seconds);
    }

    public static String formatSecondsToTime(int time) {
        int minutes = time / (60);
        int seconds = (time) % 60;
        return String.format("%d:%02d", minutes, seconds);
    }
}
