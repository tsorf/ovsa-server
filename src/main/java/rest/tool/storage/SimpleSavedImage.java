package rest.tool.storage;

/**
 * Created by ovcst on 13.04.2017.
 */
public class SimpleSavedImage implements FileManager.SavedImage {

    private String root;

    private String original;
    private String big;
    private String small;
    private String thumb;

    public SimpleSavedImage(String root) {
        this.root = root;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    @Override
    public String root() {
        return root;
    }

    @Override
    public String original() {
        return original;
    }

    @Override
    public String big() {
        return big;
    }

    @Override
    public String small() {
        return small;
    }

    @Override
    public String thumb() {
        return thumb;
    }
}
