package rest.tool.storage;

import spark.Request;

/**
 * Created by ovcst on 13.04.2017.
 */
public interface FileManager {

    String JETTY_MULTIPART_CONFIG = "org.eclipse.jetty.multipartConfig";
    String DEFAULT_FILE_PART_NAME = "file";
    int MAX_FILE_SIZE = 100 * 1000 * 1000;
    int MAX_REQUEST_SIZE = 100 * 1000 * 1000;
    int FILE_SIZE_THRESHOLD = 1024;

    SavedFile saveFile(Request request, String partName) throws Exception;
    SavedImage saveImage(Request request, String partName) throws Exception;
    void setRootPath(String path);
    void setTempPath(String path);

    interface SavedFile {
        String pathFromRoot();
        String name();
        String extension();
        long size();
    }

    interface SavedImage {
        String root();
        String original();
        String big();
        String small();
        String thumb();
    }
}
