package rest.tool.storage;

import java.io.File;

/**
 * Created by ovcst on 13.04.2017.
 */
public class SimpleSavedFile implements FileManager.SavedFile {

    private long size;
    private String name;
    private String extension;
    private String path;

    public SimpleSavedFile(File file) {
        this.size = file.getTotalSpace();
        this.name = file.getName();
        this.path = file.getPath();
        this.extension = getFileExtension(file);
    }

    private String getFileExtension(File file) {
        try {
            return file.getName().split("\\.")[1];
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public String pathFromRoot() {
        return path;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String extension() {
        return extension;
    }

    @Override
    public long size() {
        return size;
    }
}
