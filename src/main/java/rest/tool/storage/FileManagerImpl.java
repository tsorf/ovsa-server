package rest.tool.storage;

import com.google.common.hash.Hashing;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import javax.inject.Inject;
import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import data.string.R;
import exception.BadRequestException;
import exception.RestException;
import spark.Request;

/**
 * Created by ovcst on 13.04.2017.
 */
public class FileManagerImpl implements FileManager {

    private static final SimpleDateFormat DIRECTORY_FORMATTER = new SimpleDateFormat("yyyyMMdd" + File.separator);
    private static final Collection<String> IMAGE_EXTENSIONS =
            Arrays.asList("jpg", "png", "gif", "jpeg");
    private static final Random RANDOM = new SecureRandom();

    private String rootPath;
    private String tempPath;


    @Inject
    public FileManagerImpl() {
    }


    @Override
    public SavedFile saveFile(Request request, String partName) throws Exception {
        if(getRootPath(request) == null) throw new RestException("Root path is not set", 500);
        String generatedFilePath = getTempPath(request);
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(
                generatedFilePath, MAX_FILE_SIZE, MAX_REQUEST_SIZE, FILE_SIZE_THRESHOLD);
        request.raw().setAttribute(JETTY_MULTIPART_CONFIG, multipartConfigElement);

        String fileName = request.raw().getPart(partName == null ?
                DEFAULT_FILE_PART_NAME : partName).getSubmittedFileName();

        Part uploadedFile = request.raw().getPart(partName == null ?
                DEFAULT_FILE_PART_NAME : partName);
        if(uploadedFile == null)
            throw new BadRequestException(R.string.no_file);
        Path out = Paths.get(generatedFilePath + fileName);
        try (final InputStream in = uploadedFile.getInputStream()) {
            Files.copy(in, out);
            uploadedFile.delete();
        }
        return new SimpleSavedFile(new File(generatedFilePath + fileName));
    }

    @Override
    public SavedImage saveImage(Request request, String partName) throws Exception {
        if(getRootPath(request) == null) throw new RestException("Root path is not set", 500);
        String tempPath = getTempPath(request);
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(
                tempPath, MAX_FILE_SIZE, MAX_REQUEST_SIZE, FILE_SIZE_THRESHOLD);
        request.raw().setAttribute(JETTY_MULTIPART_CONFIG, multipartConfigElement);

        String fileName;
        try {
            fileName = request.raw().getPart(partName == null ?
                    DEFAULT_FILE_PART_NAME : partName).getSubmittedFileName();
        } catch (Exception e) {
            throw new BadRequestException(R.string.no_file);
        }

        Part uploadedFile = request.raw().getPart(partName == null ?
                DEFAULT_FILE_PART_NAME : partName);
        String tempFileName = tempPath + generateRandomName(getExtension(fileName));
        Path out = Paths.get(tempFileName);
        try (final InputStream in = uploadedFile.getInputStream()) {
            Files.copy(in, out);

        }
        return processImages(request, new File(out.toString()));
    }

    @Override
    public void setRootPath(String path) {
        this.rootPath = path;
    }

    @Override
    public void setTempPath(String path) {
        this.tempPath = path;
    }

    private SimpleSavedImage processImages(Request request, File original) throws Exception {
        String extension = getExtension(original.getName());
        String folderName = generateFolderName();
        File directory = new File(getRootPath(request) + folderName);
        if(!directory.exists()) directory.mkdir();
        SimpleSavedImage image = new SimpleSavedImage(getRootPath(request));

        String orig = generateRandomName(extension);
        Thumbnails.of(original).size(1800, 1800).toFile(directory.getPath() + File.separator + orig);
        image.setOriginal(folderName + orig);

        String big = generateRandomName(extension);
        Thumbnails.of(original).size(900, 900).toFile(directory.getPath() + File.separator + big);
        image.setBig(folderName + big);

        String small = generateRandomName(extension);
        Thumbnails.of(original).size(300, 300).toFile(directory.getPath() + File.separator + small);
        image.setSmall(folderName + small);

        String thumb = generateRandomName(extension);
        Thumbnails.of(original).size(100, 100).crop(Positions.CENTER).toFile(directory.getPath() + File.separator + thumb);
        image.setThumb(folderName + thumb);
        original.delete();
        return image;
    }

    private String getExtension(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i+1);
        }
        return extension;
    }

    private boolean isImage(String fileName) {
        try {
            return IMAGE_EXTENSIONS.contains(getExtension(fileName));
        } catch (Exception e) {
            return false;
        }
    }

    private String generateRandomName(String extension) {
        return Hashing.md5().hashLong(generateRandomLong()) + "." + extension;
    }

    private long generateRandomLong() {
        return System.currentTimeMillis() * (int)(Math.random() * 10000 + 1);
    }

    private String generateFolderName() {
        return DIRECTORY_FORMATTER.format(new Date());
    }

    protected String getTempPath(Request request) {
        return tempPath == null ? WIN_TEMP : tempPath;
    }

    protected String getRootPath(Request request) {
        return rootPath == null ? WIN_ROOT : rootPath;
    }

    private static final String WIN_TEMP = "C:/Users/ovcst/DEV/sandbox/uploads/temp/";
    private static final String WIN_ROOT = "C:/Users/ovcst/DEV/sandbox/uploads/userpics/";
}
