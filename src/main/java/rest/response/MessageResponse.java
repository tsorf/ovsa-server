package rest.response;

/**
 * Created by ovcst on 29.01.2017.
 */
public class MessageResponse {
    private String message;
    private int code;

    public MessageResponse(String message, int code) {
        this.message = message;
        this.code = code;
    }
}
