package rest.response;

import java.util.List;

/**
 * Created by ovcst on 31.01.2017.
 */
public class ListResponse {
    private List items;
    private Meta meta;

    public ListResponse(List items) {
        this.items = items;
        meta = new Meta(items.size());
    }

    public static class Meta {
        private int count;

        public Meta(int count) {
            this.count = count;
        }
    }
}
