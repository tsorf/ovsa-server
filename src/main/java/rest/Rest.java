package rest;

import exception.BadRequestException;
import exception.NotAuthorizedException;
import exception.NotFoundException;
import exception.OptionsAvailableException;
import exception.RestException;
import rest.response.MessageResponse;
import spark.Response;
import spark.route.RouteOverview;

import static data.serializer.Serializer.toJson;
import static spark.Spark.*;
import static spark.Spark.after;
import static spark.Spark.exception;
import static spark.Spark.setPort;

/**
 * Created by ovcst on 29.01.2017.
 */
public abstract class Rest {
    public Rest() {
        initServer();
        initControllers();
        initExchange();
    }

    protected void initServer() {
        try {
            port(getPort());
        } catch (Exception e) {

        }
    }

    protected int getPort() {
        return 4567;
    };

    protected void initControllers() {
        System.out.println("Controllers are not initialized. Assumed di");
    };

    protected void initExchange() {
        RouteOverview.enableRouteOverview();
        exception(NotFoundException.class, (e, request, response) -> {
            fillErrorResponse(e, 404, response);
        });
        exception(BadRequestException.class, (e, request, response) -> {
            fillErrorResponse(e, 400, response);
        });
        exception(NotAuthorizedException.class, (e, request, response) -> {
            fillErrorResponse(e, 401, response);
        });
        exception(OptionsAvailableException.class, (e, request, response) -> {
            fillErrorResponse(e, 422, response);
        });
        exception(RestException.class, (e, request, response) -> {
            RestException exception = ((RestException) e);
            fillErrorResponse(exception, exception.getCode(), response);
        });
        exception(Exception.class, (e, request, response) -> {
            fillErrorResponse(e, 500, response);
        });
        get("*", (request, response) -> {
            //temp bugfix for socket routes
            if(request.raw().getPathInfo().contains("socket")) return null;
            fillErrorResponse(new NotFoundException("route not found"), 404, response);
            return response;
        });
        post("*", (request, response) -> {
            fillErrorResponse(new NotFoundException("route not found"), 404, response);
            return response;
        });
        after((request, response) -> response.type("application/json"));
        after("/debug/routeoverview/", (request, response) -> {
            response.type("text/html");
        });

    }

    private void fillErrorResponse(Exception e, int code, Response response) {
        MessageResponse messageResponse = new MessageResponse(
                e.getMessage(), code
        );
        response.body(toJson(messageResponse));
        response.status(code);
        response.type("application/json");
    }
}
