package rest.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import chat.contract.entity.IChat;
import chat.contract.entity.IChatMember;
import chat.contract.repo.service.IChatService;
import chat.impl.base.mapped.ChatResult;
import chat.impl.base.repo.DefaultCreateChatRequest;
import chat.impl.variant.websocket.entity.mapper.MappersProvider;
import chat.impl.variant.websocket.handler.ChatSocketHandler;
import data.service.BaseService;
import spark.Request;
import spark.Response;
import spark.Route;

import static data.serializer.Serializer.json;
import static spark.Spark.*;
import static spark.Spark.webSocket;

/**
 * Created by ovcst on 08.04.2017.
 */

/**
 * Websocket routes must be initialized first!
 * @param <T>
 */

public abstract class ChatController<T extends BaseService & IChatService> extends OvsaController<T> {

    private ChatSocketHandler socketHandler;
    public ChatController(T service, ChatSocketHandler socketHandler) {
        super(service);
        this.socketHandler = socketHandler;
        MappersProvider mappersProvider = getMappers();
        if(mappersProvider != null) socketHandler.setMappersProvider(mappersProvider);
        initChatRoutes();
    }

    protected abstract MappersProvider getMappers();

    private void initChatRoutes() {
        webSocket(getBaseChatPath() + "socket", socketHandler);
        get(getBaseChatPath() + "list", (request, response) -> loadChats(request), json());
        post(getBaseChatPath() + "createChat", (request, response) -> createChat(request), json());
    }

    protected Object createChat(Request request) {
        IChatMember creator = findAuthorizedIdentity(request);
        DefaultCreateChatRequest createChatRequest =
                parseRequestModel(request, DefaultCreateChatRequest.class);
        IChat chat = service.createChat(createChatRequest, creator);
        return chat.map();
    }

    protected Object loadChats(Request request) {
        IChatMember creator = findAuthorizedIdentity(request);
        List<IChat> chats = service.loadChatsForMember(creator);

        List<ChatResult> result = new ArrayList<>();
        chats.forEach(chat -> result.add(chat.map().preparedForUser(creator)));
        return wrapList(result);
    }

    protected abstract IChatMember findAuthorizedIdentity(Request request);

    protected abstract String getBaseChatPath();
}
