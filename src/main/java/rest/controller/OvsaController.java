package rest.controller;

import java.lang.reflect.Method;
import java.util.List;

import data.service.BaseService;
import exception.BadRequestException;
import rest.request.BaseRequest;
import rest.response.ListResponse;
import spark.Request;

import static data.serializer.Serializer.fromJson;

/**
 * Created by ovcst on 29.01.2017.
 */
public abstract class OvsaController<T extends BaseService> {

    protected T service;

    public OvsaController(T service) {
        this.service = service;
        this.initRoutes();
    }

    protected ListResponse wrapList(List items) {
        return new ListResponse(items);
    }

    protected abstract void initRoutes();


    protected <Model> Model parseRequestModel(Request request, Class<Model> clazz) {
        Model model = fromJson(request.body(), clazz);
        if(model == null) throw new BadRequestException("Пустое тело запроса");
        try {
            Method method = model.getClass().getDeclaredMethod("get400Message", null);

            String requestError = ((String) method.invoke(model));
            if(requestError != null) throw new BadRequestException(requestError);
        } catch (Exception e) {
            throw new BadRequestException("Объект запроса должен содержать метод get400Message()");
        }
        return model;
    }

    protected int extractIntParam(Request request, String param) {
        try {
            return Integer.parseInt(request.params(param));
        } catch (Exception e) {
            throw new BadRequestException("Ошибка в пути ресурса: " + param + " не существует или не int");
        }
    }

    protected int extractIntQueryParam(Request request, String param) {
        try {
            return Integer.parseInt(request.queryParams(param));
        } catch (Exception e) {
            throw new BadRequestException("Ошибка в query-параметре: " + param + " не существует или не int");
        }
    }
}
