package exception.logger;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by ovcst on 12.04.2017.
 */
public class Log {
    public static final String NAME = "Logger";
    private static final Logger LOGGER = Logger.getLogger(NAME);

    public static void logError(Throwable e) {
        LOGGER.log(Level.WARNING, e.getLocalizedMessage());
    }

    public static void logError(String message) {
        LOGGER.log(Level.WARNING, message);
    }

    public static void log(String message) {
        LOGGER.log(Level.INFO, message);
    }
}
