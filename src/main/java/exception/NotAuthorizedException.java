package exception;

/**
 * Created by ovcst on 06.02.2017.
 */
public class NotAuthorizedException extends RestException {
    public NotAuthorizedException() {
        super(401);
    }

    public NotAuthorizedException(String message) {
        super(message, 401);
    }

    public NotAuthorizedException(String message, Throwable cause) {
        super(message, cause, 401);
    }

    public NotAuthorizedException(Throwable cause) {
        super(cause, 401);
    }

    public NotAuthorizedException(String message, Throwable cause, boolean enableSuppression,
                                  boolean writableStackTrace, int code) {
        super(message, cause, enableSuppression, writableStackTrace, code);
    }
}
