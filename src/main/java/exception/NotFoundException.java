package exception;

/**
 * Created by ovcst on 29.01.2017.
 */
public class NotFoundException extends RestException {



    public NotFoundException(String message) {
        super(message, 404);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause, 404);
    }

    public NotFoundException(Throwable cause) {
        super(cause, 404);
    }

    public NotFoundException(String message, Throwable cause, boolean enableSuppression,
                             boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, 404);
    }
}
