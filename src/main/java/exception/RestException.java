package exception;

/**
 * Created by ovcst on 29.01.2017.
 */
public class RestException extends RuntimeException {

    private int code;

    public RestException(int code) {
        this.code = code;
    }

    public RestException(String message, int code) {
        super(message);
        this.code = code;
    }

    public RestException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }

    public RestException(Throwable cause, int code) {
        super(cause);
        this.code = code;
    }

    public RestException(String message, Throwable cause, boolean enableSuppression,
                         boolean writableStackTrace, int code) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
