package data.service;

import com.google.common.hash.Hashing;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Random;

import data.hibernate.HibernateSessionFactory;
import exception.RestException;

/**
 * Created by ovcst on 23.03.2017.
 */
public class BaseService {

    public static final String EMPTY = "";

    public static final double ZERO = 0.0f;

    public static final int TRUE = 1;
    public static final int FALSE = 0;

    protected String generateUniqueId() {
        Random random = new SecureRandom();
        return Hashing.md5()
                .hashString(new BigInteger(130, random).toString(32),
                        Charset.forName("UTF-8")).toString();

    }

    public static Timestamp now() {
        return new Timestamp(System.currentTimeMillis());
    }
}
