package data.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

import data.hibernate.HibernateSessionFactory;
import exception.NotAuthorizedException;
import exception.NotFoundException;
import exception.RestException;

/**
 * Created by ovcst on 29.01.2017.
 */
public abstract class DAOImpl<T> {

    protected Session getSession() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        return session;
    }

    public List<T> getAll() {
        Transaction tx = null;
        try(Session session = getSession()) {
            tx = session.beginTransaction();
            List<T> results = session.createCriteria(getEntityClass()).list();
            tx.commit();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) tx.rollback();
            throw new RestException(e, 500);
        }
    }

    public T findById(Integer id) throws NotFoundException {
        Transaction tx = null;
        try(Session session = getSession()) {
            tx = session.beginTransaction();
            T entity = session.get(getEntityClass(), id);
            tx.commit();
            if(entity == null)
                throw new NotFoundException(getEntityClass().getSimpleName() +
                    " with id " + id + " not found");
            return entity;
        } catch (HibernateException e) {
            e.printStackTrace();
            if (tx != null) tx.rollback();
            throw new RestException(e, 500);
        }
    }

    public Integer insert(T entity) {
        Transaction tx = null;
        try(Session session = getSession()) {
            tx = session.beginTransaction();
            Integer id = (Integer) session.save(entity);
            tx.commit();
            return id;
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) tx.rollback();
            throw new RestException(e, 500);
        }
    }

    public void delete(T entity) {
        Transaction tx = null;
        try(Session session = getSession()) {
            tx = session.beginTransaction();
            session.delete(entity);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) tx.rollback();
            throw new RestException(e, 500);
        }
    }

    public void update(T entity){
        Transaction tx = null;
        try(Session session = getSession()) {
            tx = session.beginTransaction();
            session.saveOrUpdate(entity);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) tx.rollback();
            throw new RestException(e, 500);
        }
    }

    public void merge(T entity){
        Transaction tx = null;
        try(Session session = getSession()) {
            tx = session.beginTransaction();
            session.merge(entity);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) tx.rollback();
            throw new RestException(e, 500);
        }
    }

    public interface Instruction<P> {
        void run(Session session, Result<P> out);
    }

    protected <T> Result<T> transaction(Instruction<T> runnable) {
        Transaction transaction = null;
        try (Session session = session()) {
            Result<T> res = new Result<>();
            transaction = session.beginTransaction();
            runnable.run(session, res);
            transaction.commit();
            return res;
        } catch (RestException e) {
            if(transaction != null) transaction.rollback();
            throw e;

        } catch (HibernateException e) {
            e.printStackTrace();
            if(transaction != null) transaction.rollback();
            throw new RestException(e, 500);
        }
    }

    protected Session session() {
        return HibernateSessionFactory.getSessionFactory().openSession();
    }



    public static class Result<T> {
        T data;

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }
    }

    protected abstract Class<T> getEntityClass();
}
