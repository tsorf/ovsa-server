package data.dao.contract;

import java.util.List;

/**
 * Created by ovcst on 23.03.2017.
 */
public interface DAO<T> {
    List<T> getAll();
    T findById(Integer id);
    Integer insert(T entity);
    void delete(T entity);
    void update(T entity);
    void merge(T entity);
}
