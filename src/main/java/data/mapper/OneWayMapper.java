package data.mapper;

/**
 * Created by ovcst on 26.04.2017.
 */
public interface OneWayMapper<T> {
    Object convert(T object);
}
