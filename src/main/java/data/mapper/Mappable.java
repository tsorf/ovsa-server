package data.mapper;

/**
 * Created by ovcst on 11.04.2017.
 */
public interface Mappable<T> {
    T map();
}
