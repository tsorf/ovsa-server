package data.serializer;

/**
 * Created by ovcst on 28.03.2017.
 */
public interface Mapper<T> {
    T map();
}
