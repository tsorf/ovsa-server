package data.serializer;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import spark.ResponseTransformer;

/**
 * Created by ovcst on 29.01.2017.
 */
public class Serializer {

    private static final Gson DEFAULT_GSON = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .serializeNulls()
            .create();

    public static String toJson(Object object) {
        return DEFAULT_GSON.toJson(object);
    }
    public static ResponseTransformer json() {
        return Serializer::toJson;
    }

    public static  <Model> Model fromJson(String string, Class<Model> clazz) {
        return DEFAULT_GSON.fromJson(string, clazz);
    }

}
