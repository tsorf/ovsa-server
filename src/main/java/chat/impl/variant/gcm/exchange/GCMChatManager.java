package chat.impl.variant.gcm.exchange;

import java.io.IOException;

import chat.contract.entity.IChat;
import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;
import chat.contract.manager.IChatBridge;
import chat.contract.manager.IChatManager;
import chat.impl.variant.gcm.exchange.request.GCMNotificationRequest;
import chat.impl.variant.gcm.exchange.request.GoogleApi;
import exception.RestException;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ovcst on 08.04.2017.
 */
public class GCMChatManager implements IChatManager {

    private static final String REQUEST_URL = "";

    private GoogleApi api;

    public GCMChatManager() {

        OkHttpClient client = new OkHttpClient.Builder()
                .build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(REQUEST_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create());

        this.api = builder.build().create(GoogleApi.class);
    }

    @Override
    public IChatManager.Result dispatchChatMessage(IChat chat, IChatMessage message,  IChatBridge bridge) {
        GCMNotificationRequest request = new GCMNotificationRequest(chat, message);
        try {
            return api.sendNotification(request).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RestException(e, 500);
        }
    }

    @Override
    public void onUserConnected(IChat chat, IChatMember member, IChatBridge bridge) {

    }

    @Override
    public void onUserDisconnected(IChat chat, IChatMember member) {

    }

    @Override
    public void onChatUpdate(IChat chat, IChatMessage message, IChatBridge bridge) {

    }
}
