package chat.impl.variant.gcm.exchange.response;

import chat.contract.manager.IChatManager;

/**
 * Created by ovcst on 08.04.2017.
 */
public class GCMResult implements IChatManager.Result {

    @Override
    public int totalSent() {
        return 0;
    }

    @Override
    public int successfulSent() {
        return 0;
    }

    @Override
    public int unsuccessfulSent() {
        return 0;
    }

    @Override
    public String resultMessage() {
        return null;
    }
}
