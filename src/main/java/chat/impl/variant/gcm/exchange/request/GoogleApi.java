package chat.impl.variant.gcm.exchange.request;

import chat.impl.variant.gcm.exchange.response.GCMResult;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface GoogleApi {

    @POST("")
    Call<GCMResult> sendNotification(@Body GCMNotificationRequest request);
}
