package chat.impl.variant.websocket.entity.incoming;

import chat.impl.variant.websocket.entity.payload.ConnectPayload;

/**
 * Created by ovcst on 11.04.2017.
 */
public class ConnectMessage extends IncomingMessage<ConnectPayload> {
    public ConnectMessage(String action, long timestamp, String signature) {
        super(action, timestamp, signature);
    }
}
