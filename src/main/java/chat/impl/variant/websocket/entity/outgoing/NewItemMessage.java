package chat.impl.variant.websocket.entity.outgoing;

/**
 * Created by ovcst on 27.04.2017.
 */
public class NewItemMessage extends OutgoingMessage {

    private Object item;

    public NewItemMessage(String action, String signature, String message) {
        super(action, signature, message);
    }

    public void setItem(Object message) {
        this.item = message;
    }
}
