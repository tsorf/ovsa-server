package chat.impl.variant.websocket.entity.incoming;

import chat.impl.variant.websocket.entity.payload.ChatUpdatePayload;

/**
 * Created by ovcst on 11.04.2017.
 */
public class ChatUpdateMessage extends IncomingMessage<ChatUpdatePayload> {
    public ChatUpdateMessage(String action, long timestamp, String signature) {
        super(action, timestamp, signature);
    }
}
