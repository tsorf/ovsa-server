package chat.impl.variant.websocket.entity.mapper;

/**
 * Created by ovcst on 26.04.2017.
 */
public interface MappersProvider {
    ChatAttachmentMapper attachmentMapper();
    ChatMemberMapper memberMapper();
    ChatMessageMapper messageMapper();
}
