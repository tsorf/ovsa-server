package chat.impl.variant.websocket.entity.mapper;

import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;
import data.mapper.OneWayMapper;

/**
 * Created by ovcst on 26.04.2017.
 */
public interface ChatMessageMapper extends OneWayMapper<IChatMessage> {
}
