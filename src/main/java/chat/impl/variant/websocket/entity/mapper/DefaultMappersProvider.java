package chat.impl.variant.websocket.entity.mapper;

import chat.contract.entity.IChatAttachment;

/**
 * Created by ovcst on 26.04.2017.
 */
public class DefaultMappersProvider implements MappersProvider {
    @Override
    public ChatAttachmentMapper attachmentMapper() {
        return object -> object;
    }

    @Override
    public ChatMemberMapper memberMapper() {
        return object -> object;
    }

    @Override
    public ChatMessageMapper messageMapper() {
        return object -> object;
    }
}
