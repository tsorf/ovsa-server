package chat.impl.variant.websocket.entity.outgoing;

import chat.impl.variant.websocket.entity.incoming.IncomingMessage;

/**
 * Created by ovcst on 27.04.2017.
 */
public class ChatEnterLeaveMessage extends OutgoingMessage {

    private Object user;

    public ChatEnterLeaveMessage(IncomingMessage message) {
        super(message);
    }

    public ChatEnterLeaveMessage(String action, String signature, String message) {
        super(action, signature, message);
    }

    public void setUser(Object user) {
        this.user = user;
    }
}
