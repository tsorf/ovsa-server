package chat.impl.variant.websocket.entity.outgoing;

import chat.impl.base.mapped.SocketError;
import chat.impl.variant.websocket.entity.incoming.IncomingMessage;

/**
 * Created by ovcst on 11.04.2017.
 */
public class OutgoingMessage {
    private String action;
    private long timestamp;
    private String signature;
    private String message;
    private int code = 200;
    private SocketError error;

    public OutgoingMessage(IncomingMessage message) {
        action = message.getAction();
        signature = message.getSignature();
        timestamp = System.currentTimeMillis();
    }

    public OutgoingMessage(String action, String signature, String message) {
        this.action = action;
        this.signature = signature;
        this.message = message;
        timestamp = System.currentTimeMillis();
    }

    public SocketError getError() {
        return error;
    }

    public void setError(SocketError error) {
        this.error = error;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAction() {
        return action;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getSignature() {
        return signature;
    }

    public String getMessage() {
        return message;
    }
}
