package chat.impl.variant.websocket.entity.outgoing;

import java.util.Collection;
import java.util.List;

import chat.contract.entity.IChatMessage;
import chat.impl.variant.websocket.entity.incoming.IncomingMessage;

/**
 * Created by ovcst on 11.04.2017.
 */
public class CurrentChatStateMessage extends OutgoingMessage {

    private Collection<Object> messages;
    private Collection<Object> usersOnline;

    public CurrentChatStateMessage(IncomingMessage message) {
        super(message);
    }

    public CurrentChatStateMessage(String action, String signature, String message) {
        super(action, signature, message);
    }

    public void setMessages(Collection<Object> messages) {
        this.messages = messages;
    }

    public void setUsersOnline(Collection<Object> usersOnline) {
        this.usersOnline = usersOnline;
    }
}
