package chat.impl.variant.websocket.entity.incoming;

import chat.impl.variant.websocket.entity.payload.DisconnectPayload;

/**
 * Created by ovcst on 11.04.2017.
 */
public class DisconnectMessage extends IncomingMessage<DisconnectPayload> {
    public DisconnectMessage(String action, long timestamp, String signature) {
        super(action, timestamp, signature);
    }
}
