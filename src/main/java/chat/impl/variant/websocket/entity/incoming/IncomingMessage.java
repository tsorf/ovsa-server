package chat.impl.variant.websocket.entity.incoming;

import chat.contract.manager.IChatBridge;
import chat.impl.variant.websocket.entity.payload.Payload;

/**
 * Created by ovcst on 11.04.2017.
 */
public class IncomingMessage<T> implements IChatBridge {
    private String action;
    private T payload;
    private long timestamp;
    private String token;
    private String signature;

    public IncomingMessage(String action, long timestamp, String signature) {
        this.action = action;
        this.timestamp = timestamp;
        this.signature = signature;
    }

    public String getToken() {
        return token;
    }

    public String getSignature() {
        return signature;
    }

    public String getAction() {
        return action;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
