package chat.impl.variant.websocket.entity.serializer;

/**
 * Created by ovcst on 11.04.2017.
 */
public class Actions {
    public static final String CONNECT = "connect";
    public static final String EVENT = "event";
    public static final String DISCONNECT = "disconnect";
    public static final String USER_ENTER = "userEnter";
    public static final String USER_LEAVE = "userLeave";
    public static final String NEW_ITEM = "newItem";
}
