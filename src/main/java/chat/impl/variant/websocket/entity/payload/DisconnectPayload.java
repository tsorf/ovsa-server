package chat.impl.variant.websocket.entity.payload;

import chat.impl.base.local.R;

/**
 * Created by ovcst on 11.04.2017.
 */
public class DisconnectPayload extends Payload {
    private int userId;
    private int chatId;

    public DisconnectPayload(int userId, int chatId) {
        this.userId = userId;
        this.chatId = chatId;
    }

    public int getChatId() {
        return chatId;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public String invalidMessage() {
        if(userId == 0) return R.string.no_user_id;
        if(chatId == 0) return R.string.no_chat_id;
        return null;
    }
}
