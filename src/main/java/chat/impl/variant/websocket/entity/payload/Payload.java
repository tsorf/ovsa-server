package chat.impl.variant.websocket.entity.payload;

/**
 * Created by ovcst on 12.04.2017.
 */
public abstract class Payload {
    public abstract String invalidMessage();
}
