package chat.impl.variant.websocket.entity.payload;

import java.util.ArrayList;
import java.util.List;

import chat.impl.base.local.R;

/**
 * Created by ovcst on 11.04.2017.
 */
public class ChatUpdatePayload extends Payload {
    private int chatId;
    private int userId;
    private String text;
    private List<String> attachments = new ArrayList<>();
    private int toMember;

    public int getChatId() {
        return chatId;
    }

    public int getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public int getToMember() {
        return toMember;
    }

    @Override
    public String invalidMessage() {
        if(userId == 0) return R.string.no_user_id;
        if(chatId == 0) return R.string.no_chat_id;
        return null;
    }
}
