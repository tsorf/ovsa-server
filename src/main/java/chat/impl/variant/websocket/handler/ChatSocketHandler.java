package chat.impl.variant.websocket.handler;

import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import chat.contract.entity.IChat;
import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;
import chat.contract.manager.IChatBridge;
import chat.contract.repo.service.IChatService;
import chat.impl.base.local.R;
import chat.impl.base.repo.DefaultChatLoadRequest;
import chat.impl.variant.websocket.entity.outgoing.ChatEnterLeaveMessage;
import chat.impl.variant.websocket.entity.outgoing.CurrentChatStateMessage;
import chat.impl.variant.websocket.entity.outgoing.OutgoingMessage;
import chat.impl.variant.websocket.entity.serializer.Actions;

/**
 * Created by ovcst on 11.04.2017.
 */
@WebSocket
public class ChatSocketHandler extends BaseChatSocketHandler {

    private IChatService service;

    @Inject
    public ChatSocketHandler(IChatService service) {
        super(service);
        this.service = service;
    }

    @Override
    public void onUserConnected(IChat chat, IChatMember member, IChatBridge bridge) {
        ChatEnterLeaveMessage message = new ChatEnterLeaveMessage(
                Actions.USER_ENTER, null, R.string.user_enters
        );
        message.setUser(mappers().memberMapper().convert(member));
        dispatchChatEvent(chat, message);

        CurrentChatStateMessage currentChatStateMessage = new CurrentChatStateMessage(
                bridge.getAction(),
                bridge.getSignature(), SUCCESS);

        DefaultChatLoadRequest request = new DefaultChatLoadRequest();
        Collection<IChatMessage> messages = service.loadChatHistory(chat, request);
        List<Object> mapped = new ArrayList<>();
        messages.forEach(m -> mapped.add(mappers().messageMapper().convert(m)));
        currentChatStateMessage.setMessages(mapped);

        Collection<IChatMember> onliners = getOnliners(chat, member);
        List<Object> mappedOnliners = new ArrayList<>();
        onliners.forEach(m -> mappedOnliners.add(mappers().memberMapper().convert(m)));
        currentChatStateMessage.setUsersOnline(mappedOnliners);

        dispatchUserMessage(findSessionByMember(member), currentChatStateMessage);
    }

    @Override
    public void onUserDisconnected(IChat chat, IChatMember member) {
        ChatEnterLeaveMessage message = new ChatEnterLeaveMessage(
                Actions.USER_LEAVE, null, R.string.user_leave
        );
        message.setUser(mappers().memberMapper().convert(member));
        dispatchChatEvent(chat, message);
    }

    @Override
    public void onChatUpdate(IChat chat, IChatMessage message, IChatBridge bridge) {
        service.storeNewMessage(chat, message);
        OutgoingMessage outgoingMessage = new OutgoingMessage(
                bridge.getAction(),
                bridge.getSignature(), SUCCESS);
        dispatchUserMessage(findSessionByMember(message.author()), outgoingMessage);
        dispatchChatMessage(chat, message, bridge);
    }
}
