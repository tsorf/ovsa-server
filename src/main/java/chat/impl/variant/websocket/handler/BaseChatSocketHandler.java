package chat.impl.variant.websocket.handler;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketFrame;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.api.extensions.Frame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import chat.contract.entity.IChat;
import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;
import chat.contract.manager.IChatBridge;
import chat.contract.manager.IChatManager;
import chat.contract.repo.service.IChatIdentityService;
import chat.impl.base.entity.ChatMessage;
import chat.impl.base.local.R;
import chat.impl.base.mapped.SocketError;
import chat.impl.variant.websocket.entity.incoming.ChatUpdateMessage;
import chat.impl.variant.websocket.entity.incoming.ConnectMessage;
import chat.impl.variant.websocket.entity.incoming.DisconnectMessage;
import chat.impl.variant.websocket.entity.incoming.IncomingMessage;
import chat.impl.variant.websocket.entity.mapper.DefaultMappersProvider;
import chat.impl.variant.websocket.entity.mapper.MappersProvider;
import chat.impl.variant.websocket.entity.outgoing.NewItemMessage;
import chat.impl.variant.websocket.entity.outgoing.OutgoingMessage;
import chat.impl.variant.websocket.entity.payload.Payload;
import chat.impl.variant.websocket.entity.serializer.Actions;
import chat.impl.variant.websocket.manager.DispatchResult;
import exception.BadRequestException;
import exception.RestException;
import javafx.util.Pair;

import static data.serializer.Serializer.fromJson;
import static data.serializer.Serializer.toJson;
import static data.service.BaseService.now;
import static exception.logger.Log.log;
import static exception.logger.Log.logError;

/**
 * Created by ovcst on 11.04.2017.
 */

@WebSocket
public abstract class BaseChatSocketHandler implements IChatManager {
    public static final long CLEAN_INTERVAL = 60 * 5 * 1000;

    private final Timer timer;
    private Map<IChatMember, Pair<Session, IChat>> sessionUsers = new ConcurrentHashMap<>();
    private MappersProvider mappersProvider = new DefaultMappersProvider();
    private IChatIdentityService service;

    public BaseChatSocketHandler(IChatIdentityService service) {
        this.service = service;
        timer = new Timer(true);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                selfClean();
            }
        }, now(), CLEAN_INTERVAL);
    }

    @OnWebSocketConnect
    public void onConnect(Session session) throws Exception {

    }

    @OnWebSocketFrame
    public void onFrame(Session session, Frame frame) {
        log("Frame: " + frame.toString());
    }

    @OnWebSocketClose
    public void onClose(Session session, int statusCode, String reason) {
        IChatMember member = findBySession(session);
        IChat chat = findChatByMember(member);
        sessionUsers.remove(member);
        onUserDisconnected(chat, member);
    }

    protected Collection<IChatMember> getOnliners(IChat chat, IChatMember guest) {
        List<IChatMember> onliners = new ArrayList<>();
        sessionUsers
                .keySet()
                .stream()
                .filter(member -> chat.listMembers().contains(member))
                .forEach(member -> {
                    if(member.uniqueId() != guest.uniqueId()) {
                        onliners.add(member);
                    }
                });
        return onliners;
    }

    protected MappersProvider mappers() {
        return mappersProvider;
    }

    public void setMappersProvider(MappersProvider mappersProvider) {
        this.mappersProvider = mappersProvider;
    }

    @OnWebSocketMessage
    public void onMessage(Session session, String message) {
        IncomingMessage<?> incomingMessage = null;
        try {
            incomingMessage = fromJson(message, IncomingMessage.class);
            parseActionMessage(session, incomingMessage, message);
        } catch (Exception e) {
            logError(e);
            OutgoingMessage errorMessage;
            SocketError error = new SocketError();
            error.setError(e.getMessage());
            error.setOriginalMessage(message);
            if(incomingMessage == null) {
                errorMessage = new OutgoingMessage(null, null, R.string.request_error);
                errorMessage.setCode(400);
            } else {
                errorMessage = new OutgoingMessage(incomingMessage);
                if(e instanceof RestException) {
                    errorMessage.setCode(((RestException) e).getCode());
                } else {
                    errorMessage.setCode(500);
                }
            }
            errorMessage.setMessage(e.getMessage());
            errorMessage.setError(error);
            dispatchErrorMessage(session, errorMessage);
        }
    }

    @Override
    public abstract void onUserConnected(IChat chat, IChatMember member, IChatBridge bridge);

    public abstract void onUserDisconnected(IChat chat, IChatMember member);

    @Override
    public abstract void onChatUpdate(IChat chat, IChatMessage message, IChatBridge bridge);

    @Override
    public IChatManager.Result dispatchChatMessage(IChat chat, IChatMessage message, IChatBridge bridge) {
        NewItemMessage newItemMessage = new NewItemMessage(
                Actions.NEW_ITEM, bridge.getSignature(), R.string.new_message
        );
        newItemMessage.setItem(mappers().messageMapper().convert(message));
        String preparedMessage = toJson(newItemMessage);
        DispatchResult result = new DispatchResult();
        sessionUsers
                .keySet()
                .stream()
                .filter(member -> chat.listMembers().contains(member))
                .forEach(member -> {
                    send(result, member, preparedMessage);
                    /*if(member.uniqueId() != message.author().uniqueId()) {
                        send(result, member, preparedMessage);
                    }*/
                });
        return result;
    }

    public IChatManager.Result dispatchChatEvent(IChat chat, OutgoingMessage message) {
        String preparedMessage = toJson(message);
        DispatchResult result = new DispatchResult();
        sessionUsers
                .keySet()
                .stream()
                .filter(member -> chat.listMembers().contains(member))
                .forEach(member -> send(result, member, preparedMessage));
        return result;
    }

    private DispatchResult send(DispatchResult result, IChatMember member, String message) {
        try {
            sessionUsers.get(member).getKey().getRemote().sendString(message);
            result.incrementSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            result.incrementUnsuccessful();
        }
        result.incrementTotal();
        return result;
    }

    protected IChatManager.Result dispatchUserMessage(Session session, OutgoingMessage message) {
        DispatchResult result = new DispatchResult(1);
        if(session == null || !session.isOpen()) {
            result.incrementUnsuccessful();
            result.setMessage("Session is dead or not found");
            return result;
        }
        try {
            session.getRemote().sendString(toJson(message));
            result.incrementSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            result.setMessage(e.getMessage());
        }
        return result;
    }

    protected IChatManager.Result dispatchErrorMessage(Session session, OutgoingMessage message) {
        return dispatchUserMessage(session, message);
    }

    private void parseActionMessage(Session session, IncomingMessage<?> result, String message) throws Exception {
        switch (result.getAction()) {
            case Actions.CONNECT:
                ConnectMessage connectMessage = parseWithPayloadOrThrow(message, ConnectMessage.class);
                processConnect(session, connectMessage);
                break;
            case Actions.DISCONNECT:
                DisconnectMessage disconnectMessage = parseWithPayloadOrThrow(message, DisconnectMessage.class);
                processDisconnect(session, disconnectMessage);
                break;
            case Actions.EVENT:
                ChatUpdateMessage updateMessage = parseWithPayloadOrThrow(message, ChatUpdateMessage.class);
                processChatUpdate(session, updateMessage);
                break;
        }
    }

    private <T extends IncomingMessage> T parseWithPayloadOrThrow(String message, Class<T> clazz) {
        T result = fromJson(message, clazz);
        if(result.getPayload() instanceof Payload) {
            if(result.getPayload() == null)
                throw new BadRequestException(R.string.request_payload_error);
            if(((Payload) result.getPayload()).invalidMessage() != null)
                throw new BadRequestException(((Payload) result.getPayload()).invalidMessage());
            return result;
        } else {
            throw new BadRequestException(R.string.bad_message);
        }
    }

    private void processConnect(Session session, ConnectMessage message) throws Exception {
        IChatMember member = service.identifyMember(message.getPayload().getUserId(), message.getToken());
        IChat chat = service.identifyChat(message.getPayload().getChatId());
        assertEverythingIsAlright(session, member, chat, message);
        sessionUsers.put(member, new Pair<>(session, chat));
        onUserConnected(chat, member, message);
    }

    private void processDisconnect(Session session, DisconnectMessage message) throws Exception {
        IChatMember member = service.identifyMember(message.getPayload().getUserId(), message.getToken());
        IChat chat = service.identifyChat(message.getPayload().getChatId());
        assertEverythingIsAlright(session, member, chat, message);
        sessionUsers.remove(member);
        onUserDisconnected(chat, member);
    }

    private void processChatUpdate(Session session, ChatUpdateMessage message) throws Exception {
        IChatMember member = service.identifyMember(message.getPayload().getUserId(), message.getToken());
        IChat chat = service.identifyChat(message.getPayload().getChatId());
        assertEverythingIsAlright(session, member, chat, message);
        ChatMessage chatMessage = new ChatMessage(member, IChatMessage.Status.NOT_SENT,
                message.getPayload().getText());
        onChatUpdate(chat, chatMessage, message);

    }

    protected void assertEverythingIsAlright(Session session, IChatMember member,
                                             IChat chat, IncomingMessage message) {
        try {
            boolean allowed = service.assertUserAllowedToChat(chat, member, message.getToken());
            if(!allowed) {
                OutgoingMessage outgoingMessage = new OutgoingMessage(message);
                outgoingMessage.setMessage("Not allowed");
                session.getRemote().sendString(toJson(outgoingMessage));
                session.close();
                sessionUsers.remove(member);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private IChatMember findBySession(Session session) {
        for(Map.Entry<IChatMember, Pair<Session, IChat>> entry: sessionUsers.entrySet()) {
            if(entry.getValue().getKey() == session) return entry.getKey();
        }
        return null;

    }

    protected Session findSessionByMember(IChatMember member) {
        return sessionUsers.get(member).getKey();
    }

    protected IChat findChatByMember(IChatMember member) {
        return sessionUsers.get(member).getValue();
    }

    protected void selfClean() {
        sessionUsers.keySet().removeIf(item -> !sessionUsers.get(item).getKey().isOpen());
    }
}
