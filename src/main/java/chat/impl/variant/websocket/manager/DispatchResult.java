package chat.impl.variant.websocket.manager;

import chat.contract.manager.IChatManager;

/**
 * Created by ovcst on 11.04.2017.
 */
public class DispatchResult implements IChatManager.Result {

    private int totalSent;
    private int successfulSent;
    private int unsucessfulSent;
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public DispatchResult() {
    }

    public DispatchResult(int totalSent) {
        this.totalSent = totalSent;
    }

    public void incrementTotal() {
        totalSent++;
    }


    public void incrementUnsuccessful() {
        unsucessfulSent++;
    }


    public void incrementSuccessful() {
        successfulSent++;
    }

    @Override
    public int totalSent() {
        return totalSent;
    }

    @Override
    public int successfulSent() {
        return successfulSent;
    }

    @Override
    public int unsuccessfulSent() {
        return unsucessfulSent;
    }

    @Override
    public String resultMessage() {
        if(message != null) return message;
        return String.format("Total: %d, success: %d, failure: %d", totalSent, successfulSent, unsucessfulSent);
    }
}
