package chat.impl.base.local;

/**
 * Created by ovcst on 11.04.2017.
 */
public class R {

    public static final class string {
        public static final String no_chat_users = "No users";
        public static final String request_error = "Request parse error";
        public static final String request_payload_error = "Empty exchange.request payload";
        public static String no_user_id = "No user id";
        public static String no_chat_id = "No chat id";
        public static String bad_message = "Incorrect message";
        public static String no_token = "No token provided";
        public static String pin_is_not_set = "Pin is not set";
        public static String user_leave = "User left the chat";
        public static String user_enters = "User enters the chat";
        public static String new_message = "New message received";
    }
}
