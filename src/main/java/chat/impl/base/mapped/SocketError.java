package chat.impl.base.mapped;

/**
 * Created by ovcst on 12.04.2017.
 */
public class SocketError {
    private String error;
    private String originalMessage;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getOriginalMessage() {
        return originalMessage;
    }

    public void setOriginalMessage(String originalMessage) {
        this.originalMessage = originalMessage;
    }
}
