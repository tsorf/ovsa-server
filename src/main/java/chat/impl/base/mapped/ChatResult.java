package chat.impl.base.mapped;

import java.util.Date;
import java.util.List;

import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;
import chat.impl.base.entity.ChatMessage;

/**
 * Created by ovcst on 11.04.2017.
 */
public class ChatResult<User extends IChatMember> {
    private int id;
    private String title;
    private Date createdAt;
    private Date updatedAt;
    private String lastMessage;
    private List<User> members;

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public ChatResult preparedForUser(IChatMember member) {
        if(members.size() == 2) {
            if(members.get(0).uniqueId() == member.uniqueId()) {
                setTitle(members.get(1).formattedName());
                return this;
            }
            if(members.get(1).uniqueId() == member.uniqueId()) {
                setTitle(members.get(0).formattedName());
                return this;
            }
            setTitle(member.formattedName());
            return this;
        } else {
            if(title != null && !title.isEmpty()) return this;
            StringBuilder builder = new StringBuilder();
            members.forEach(mem -> builder.append(", ").append(mem.formattedName()));
            setTitle(builder.toString().substring(2));
            return this;
        }
    }
}
