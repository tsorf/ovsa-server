package chat.impl.base.mapped;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import chat.contract.entity.IChatAttachment;
import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;

/**
 * Created by ovcst on 12.04.2017.
 */
public class ChatMessageResult<User> implements IChatMessage {
    private String text;
    private Date createdAt;
    private User author;

    public ChatMessageResult(String text, Date createdAt, User author) {
        this.text = text;
        this.createdAt = createdAt;
        this.author = author;
    }

    @Override
    public IChatMember author() {
        if(author instanceof IChatMember) {
            return ((IChatMember) author);
        }
        return null;
    }

    @Override
    public Status status() {
        return null;
    }

    @Override
    public List<IChatAttachment> listAttachments() {
        return new ArrayList<>();
    }

    @Override
    public String text() {
        return text;
    }

    @Override
    public Date date() {
        return createdAt;
    }
}
