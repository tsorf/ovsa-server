package chat.impl.base.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import chat.contract.entity.IChatAttachment;
import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;
import chat.impl.base.mapped.ChatMessageResult;

/**
 * Created by ovcst on 11.04.2017.
 */
public class ChatMessage implements IChatMessage {

    private IChatMember author;
    private List<IChatAttachment> attachments = new ArrayList<>();
    private Status status;
    private String text;
    private Date date = new Date();

    public ChatMessage(IChatMember author, Status status, String text) {
        this.author = author;
        this.status = status;
        this.text = text;
    }

    @Override
    public IChatMember author() {
        return author;
    }

    @Override
    public Status status() {
        return status;
    }

    @Override
    public List<IChatAttachment> listAttachments() {
        return attachments;
    }

    @Override
    public String text() {
        return text;
    }

    @Override
    public Date date() {
        return date;
    }

    public void setAuthor(IChatMember author) {
        this.author = author;
    }

    public void setAttachments(List<IChatAttachment> attachments) {
        this.attachments = attachments;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
