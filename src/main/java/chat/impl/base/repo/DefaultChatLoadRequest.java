package chat.impl.base.repo;

import java.util.Date;

import chat.contract.repo.request.IChatLoadRequest;

/**
 * Created by ovcst on 08.04.2017.
 */
public class DefaultChatLoadRequest implements IChatLoadRequest {

    private Date dateFrom;
    private int offset;
    private int count;

    public DefaultChatLoadRequest() {
        count = 30;
    }

    public DefaultChatLoadRequest(Date dateFrom, int offset, int count) {
        this.dateFrom = dateFrom;
        this.offset = offset;
        this.count = count;
    }

    @Override
    public Date dateFrom() {
        return dateFrom;
    }

    @Override
    public int offset() {
        return offset;
    }

    @Override
    public int count() {
        return count;
    }
}
