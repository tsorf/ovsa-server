package chat.impl.base.repo;

import com.sun.istack.internal.Nullable;

import java.util.ArrayList;
import java.util.List;

import chat.contract.entity.IChatMember;
import chat.contract.repo.request.IChatCreateRequest;
import chat.impl.base.local.R;
import rest.request.BaseRequest;

/**
 * Created by ovcst on 08.04.2017.
 */
public class DefaultCreateChatRequest extends BaseRequest implements IChatCreateRequest {

    private String title;
    private List<Integer> members = new ArrayList<>();

    public DefaultCreateChatRequest(String title, List<Integer> members) {
        this.title = title;
        this.members = members;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public List<Integer> ids() {
        return members;
    }

    @Override
    public String get400Message() {
        if(members.isEmpty()) return R.string.no_chat_users;
        return null;
    }
}
