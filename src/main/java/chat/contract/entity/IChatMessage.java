package chat.contract.entity;

import java.util.Date;
import java.util.List;

import chat.contract.entity.IChatAttachment;
import chat.contract.entity.IChatMember;
import chat.impl.base.mapped.ChatMessageResult;
import data.mapper.Mappable;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface IChatMessage {

    IChatMember author();
    Status status();
    List<IChatAttachment> listAttachments();
    String text();
    Date date();

    enum Status {
        NOT_SENT, SENT, READ
    }
}
