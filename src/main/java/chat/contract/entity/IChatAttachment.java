package chat.contract.entity;

import java.util.List;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface IChatAttachment {
    Type type();

    enum Type {
        PHOTO, AUDIO, VIDEO, FILE, LINK, TASK
    }
}
