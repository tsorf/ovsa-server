package chat.contract.entity;

import data.mapper.Mappable;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface IChatMember  {
    String gcmNotificationKey();
    int uniqueId();
    String formattedName();
}
