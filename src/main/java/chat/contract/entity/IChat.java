package chat.contract.entity;

import java.util.Collection;
import java.util.List;

import chat.impl.base.mapped.ChatResult;
import data.mapper.Mappable;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface IChat extends Mappable<ChatResult<?>> {
    Collection<IChatMember> listMembers();
    int uniqueId();
}
