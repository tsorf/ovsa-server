package chat.contract.manager;

/**
 * Created by ovcst on 11.04.2017.
 */
public interface IChatBridge {
    String getAction();
    String getSignature();
}
