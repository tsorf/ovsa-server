package chat.contract.manager;

import chat.contract.entity.IChat;
import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface IChatManager {

    String FAILURE = "failure";
    String SUCCESS = "success";

    Result dispatchChatMessage(IChat chat, IChatMessage message, IChatBridge bridge);
    void onUserConnected(IChat chat, IChatMember member, IChatBridge bridge);
    void onUserDisconnected(IChat chat, IChatMember member);
    void onChatUpdate(IChat chat, IChatMessage message, IChatBridge bridge);

    interface Result {
        int totalSent();
        int successfulSent();
        int unsuccessfulSent();
        String resultMessage();
    }
}
