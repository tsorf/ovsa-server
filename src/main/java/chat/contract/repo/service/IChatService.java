package chat.contract.repo.service;

import java.util.Collection;
import java.util.List;

import chat.contract.entity.IChat;
import chat.contract.entity.IChatMember;
import chat.contract.entity.IChatMessage;
import chat.contract.repo.request.IChatCreateRequest;
import chat.contract.repo.request.IChatLoadRequest;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface IChatService extends IChatIdentityService {
    List<IChat> loadChatsForMember(IChatMember member);
    Collection<IChatMessage> loadChatHistory(IChat chat, IChatLoadRequest request);
    IChatMessage storeNewMessage(IChat chat, IChatMessage message);
    IChat createChat(IChatCreateRequest request, IChatMember creator);
    boolean deleteMessage(IChatMessage message);
}
