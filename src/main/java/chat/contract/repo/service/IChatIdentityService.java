package chat.contract.repo.service;

import chat.contract.entity.IChat;
import chat.contract.entity.IChatMember;
import exception.BadRequestException;
import exception.NotAuthorizedException;
import exception.NotFoundException;

/**
 * Created by ovcst on 11.04.2017.
 */
public interface IChatIdentityService {
    IChatMember identifyMember(int memberId, String token) throws Exception;
    IChat identifyChat(int chatId) throws NotFoundException;
    boolean assertUserAllowedToChat(IChat chat, IChatMember member, String token);
}
