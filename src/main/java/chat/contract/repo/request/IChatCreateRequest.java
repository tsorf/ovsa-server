package chat.contract.repo.request;

import java.util.List;

import chat.contract.entity.IChatMember;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface IChatCreateRequest {
    String title();
    List<Integer> ids();
}
