package chat.contract.repo.request;

import java.util.Date;

/**
 * Created by ovcst on 08.04.2017.
 */
public interface IChatLoadRequest {
    Date dateFrom();
    int offset();
    int count();
}
